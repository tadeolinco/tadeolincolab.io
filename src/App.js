import React from 'react';
import { Router } from '@reach/router';
import QuizPage from './pages/QuizPage/QuizPage';
import QuizLogin from './pages/QuizLogin/QuizLogin';

const App = () => {
  return (
    <Router>
      <QuizLogin path="/" />
      <QuizPage path="/quiz/:quizId" />
    </Router>
  );
};

export default App;

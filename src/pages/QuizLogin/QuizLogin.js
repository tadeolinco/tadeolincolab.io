import React, { useState } from 'react';
import './style.scss';

const QuizLogin = ({ navigate }) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [code, setCode] = useState('');

  const handleOnSubmit = event => {
    event.preventDefault();
    navigate(`/quiz/${code}`);
  };

  return (
    <div className="quiz-login-page">
      <h4>Test Your Knowledge</h4>
      <div className="form-container">
        <form
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
          onSubmit={handleOnSubmit}
        >
          <div>
            <label htmlFor="name">Name</label>
            <input
              name="name"
              value={name}
              onChange={event => setName(event.target.value)}
            />
          </div>
          <div>
            <label htmlFor="email">Email</label>
            <input
              name="email"
              type="email"
              value={email}
              onChange={event => setEmail(event.target.value)}
            />
          </div>
          <div>
            <label htmlFor="code">Code</label>
            <input
              name="code"
              value={code}
              onChange={event => setCode(event.target.value)}
            />
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              marginTop: '0.5em',
            }}
          >
            <h6 style={{ textAlign: 'center', marginBottom: '0.5em' }}>
              Ready. Set.
            </h6>
            <button
              type="submit"
              style={{
                backgroundColor: '#ff8738',
                padding: '0.5em',
                color: 'white',
                borderRadius: 5,
              }}
              disabled={!name || !email || !code}
            >
              GO!
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default QuizLogin;

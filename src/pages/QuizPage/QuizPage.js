import React, { useState } from 'react';
import questions from './questions.json';
import Question from './Question';
import './style.scss';

const QuizPage = () => {
  const [answers, setAnswers] = useState(questions.map(() => null));
  const [hasSubmitted, setHasSubmitted] = useState(false);
  const [startTime] = useState(new Date().getTime());

  return (
    <div className="quiz-page">
      <div className="banner">
        <img src={require('../../assets/banking.jpg')} alt="" />
        <div className="division-container">
          <h4>Consumer Banking</h4>
        </div>
      </div>
      <div style={{ flex: 1 }} />
      <div className="quiz-container">
        {!hasSubmitted ? (
          questions.map((question, index) => (
            <Question
              {...question}
              index={index}
              answers={answers}
              setAnswers={setAnswers}
              isLast={index === questions.length - 1}
              setHasSubmitted={setHasSubmitted}
            />
          ))
        ) : (
          <div className="score-container">
            <h1>
              {answers.reduce((acc, curr, index) => {
                if (questions[index].answer === curr) {
                  return acc + 1;
                }
                return acc;
              }, 0)}
              /{answers.length}
            </h1>
            <h4>
              Time: {((new Date().getTime() - startTime) / 1000).toFixed(2)}{' '}
              seconds
            </h4>
            {answers.map((answer, index) => (
              <div
                key={index}
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  marginTop: '1em',
                }}
              >
                <div style={{ marginRight: '1em' }}>
                  <h6>#{index + 1}</h6>
                </div>
                <div style={{ marginLeft: '1em' }}>
                  {answer === questions[index].answer ? '✅' : '❌'}
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default QuizPage;

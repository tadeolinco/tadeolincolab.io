import React from 'react';
import classnames from 'classnames';

const Question = ({
  question,
  choices,
  index,
  answers,
  setAnswers,
  setHasSubmitted,
  isLast,
}) => {
  return (
    <>
      <div className="question-container">
        <h6 className="question-number">QUESTION {index + 1}</h6>
        <h4 className="question">{question}</h4>
        <div className="choices-container">
          {choices.map((choice, choiceIndex) => (
            <div
              key={choice}
              className={classnames('choice', {
                answer: choiceIndex === answers[index],
              })}
              onClick={() => {
                setAnswers(
                  answers.map((answer, answerIndex) =>
                    answerIndex === index ? choiceIndex : answer
                  )
                );
              }}
            >
              {choice}
            </div>
          ))}
          <button
            className="next"
            disabled={answers[index] === null}
            onClick={() => {
              if (!isLast) {
                const el = document.getElementById('question-' + (index + 1));
                if (el) {
                  el.scrollIntoView({ behavior: 'smooth', block: 'end' });
                }
              } else {
                setHasSubmitted(true);
              }
            }}
          >
            {isLast ? 'SUBMIT' : 'NEXT'}
          </button>
        </div>
      </div>
      <div id={'question-' + index} />
    </>
  );
};

export default Question;
